import React from 'react';
import styles from './CSSModule.module.scss'

const Cssmodule = () => {
  return (
    <div className={`${styles.wrapper} ${styles.inverted}`}>
      안녕하세요. 저는 <span className='something'>CSS MODULE!</span>
    </div>
  );
}

export default Cssmodule;
