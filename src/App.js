import React, { Component } from 'react';
import StyledComponents from './StyledComponents.js';

class App extends Component {
  render() {
    return <StyledComponents/>
  }
}

export default App;
